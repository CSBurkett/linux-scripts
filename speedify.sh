#!/bin/bash

if [[ -z "$1" ]]; then
    echo "Must provide tempo in environment" 1>&2
    exit 1
fi

for i in Workshop/*
do
	if [[ $i == *.mp3 ]]
	then
		ffmpeg -i "$i" -filter:a "atempo=$1" "${i%.mp3}_$1x.mp3"
#		rm $i # Remove file if prompted
	fi
done
